import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PqGridRoutingModule } from './pq-grid-routing.module';
import { PqGridComponent } from './pq-grid/pq-grid.component';


@NgModule({
  declarations: [
    PqGridComponent
  ],
  imports: [
    CommonModule,
    PqGridRoutingModule,
    SharedModule
  ]
})
export class PqGridModule { }
