import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PqGridComponent } from './pq-grid.component';

describe('PqGridComponent', () => {
  let component: PqGridComponent;
  let fixture: ComponentFixture<PqGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PqGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PqGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
