import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pq-grid',
  templateUrl: './pq-grid.component.html',
  styleUrls: ['./pq-grid.component.scss']
})
export class PqGridComponent implements OnInit {
  public columns;
  public rowData=[
    { rank: 1, 
      company: "Exxon Mobil", 
      revenues: 339938.0, 
      profits: 36130.0 
    },
    {
      rank: 2,
      company: "Wal-Mart Stores",
      revenues: 315654.0,
      profits: 11231.0
    },
    {
      rank: 3,
      company: "Royal Dutch Shell",
      revenues: 306731.0,
      profits: 25311.0
    },
    { rank: 4, 
      company: "BP", 
      revenues: 267600.0, 
      profits: 22341.0 
    },
    {
      rank: 5,
      company: "General Motors",
      revenues: 192604.0,
      profits: -10567.0
    },
    { rank: 6, 
      company: "Chevron", 
      revenues: 189481.0, 
      profits: 14099.0 
    },
    {
      rank: 7,
      company: "DaimlerChrysler",
      revenues: 186106.3,
      profits: 3536.3
    },
    { rank: 8, 
          company: "Toyota Motor", 
          revenues: 185805.0, 
          profits: 12119.6 
        },
    { rank: 9, 
          company: "Ford Motor", 
          revenues: 177210.0, 
          profits: 2024.0 
        },
    {
      rank: 10,
      company: "ConocoPhillips",
      revenues: 166683.0,
      profits: 13529.0
    },
    {
      rank: 11,
      company: "General Electric",
      revenues: 157153.0,
      profits: 16353.0
    },
    { rank: 12, 
          company: "Total", 
          revenues: 152360.7, 
          profits: 15250.0 
        },
    { rank: 13, 
         company: "ING Group", 
         revenues: 138235.3, 
         profits: 8958.9 
        },
    { rank: 14, 
          company: "Citigroup", 
          revenues: 131045.0, 
          profits: 24589.0 
        },
    { rank: 15, 
          company: "AXA", 
          revenues: 129839.2, 
          profits: 5186.5 
        },
    { rank: 16, 
          company: "Allianz", 
          revenues: 121406.0, 
          profits: 5442.4 
        },
    { rank: 17, 
          company: "Volkswagen", 
          revenues: 118376.6, 
          profits: 1391.7 
        },
    { rank: 18, 
          company: "Fortis", 
          revenues: 112351.4, 
          profits: 4896.3 
        },
    {
      rank: 19,
      company: "Crédit Agricole",
      revenues: 110764.6,
      profits: 7434.3
    },
    {
      rank: 20,
      company: "American Intl. Group",
      revenues: 108905.0,
      profits: 10477.0
    }
  ];
  constructor() { }

  ngOnInit(): void {
    this.manageColumns()
  }
  manageColumns() {
    this.columns = [
      this.manageEachColumn(
        'Rank',
        'rank',
        true,
        true,
        'number',
        false,
        null,
        { width: 100 }
      ),
      this.manageEachColumn(
        'Company',
        'company',
        true,
        false,
        'string',
        false,
        null,
        { width: 200 }
      ),
      this.manageEachColumn(
        "Revenues",
        'revenues',
        true,
        false,
        'number',
        false,
        null,
        { width: 100 }
      ),
      this.manageEachColumn(
        'Profits',
        'profits',
        true,
        false,
        'number',
        false,
        null,
        { width: 100 }
      ),
      this.manageEachColumn('Action', '', false, false, '', true, this),
    ];
  }


  manageEachColumn(
    title,
    dataIndx,
    filter: boolean = false,
    sortable: boolean = true,
    dataType: string = 'string',
    action: any = false,
    that: any = [],
    gridConfig: object = {},
    align?
  ): Object {
    let columns: Object = {
      title: title,
      dataType: dataType,
      dataIndx: dataIndx,
      editable: false,
      sortable: sortable,
      halign: 'center',
      align: align, // set header align center
    };

    if (filter) {
      columns['filter'] = {
        crules: [{ condition: 'contain' }],
        listeners: [
          {
            change: function (evt, ui) {
              let filterRules = [
                {
                  dataIndx: ui.dataIndx,
                  condition: 'contain',
                  value: ui.value,
                },
              ];
              this.filter({
                oper: 'add',
                rules: filterRules,
              });
            },
          },
        ],
      };
    }
    let here = this;
    if (action) {
      columns['width'] = 200;
      columns['maxWidth'] = 300;
      columns['align'] = 'center';
      columns['render'] = (ui) => {
        return this.pqGridActionButtons(action, ui, that, gridConfig);
      };
      columns['postRender'] = function (ui) {
        var grid = this,
          $cell = grid.getCell(ui);
        let selectedRow = gridConfig['useRowData'] ? ui.rowData : ui.rowData;
        /**
         * view button
         **/
        $cell
          .find('.view_btn')
          .button({ icons: '' })
          .off('click')
          .on('click', function (evt) {
            here.view(selectedRow);
          });
        /**
         * edit button
         **/
        $cell
          .find('.edit_btn')
          .button({ icons: '' })
          .off('click')
          .on('click', function (evt) {
            here.view(selectedRow);
          });

        /**
         * delete button
         **/
        $cell
          .find('.delete_btn')
          .button({ icons: '' })
          .off('click')
          .on('click', function (evt) {
            here.view(selectedRow);
          });
      };
    }

    if (Object.keys(gridConfig).length > 0) {
      Object.assign(columns, gridConfig);
    }
    return columns;
  }

  pqGridActionButtons(action, ui, that, config) {
    let edit = `<button type="button" class="edit_btn" mat-button>edit</button>`;
    let drop = `<button type="button" class="delete_btn" mat-button>delete</button>`;
    let view = `<button type="button" class="view_btn" mat-button>view</button>`;
    
    return edit + drop + view;
  }

  view(selectedRow){
      console.log(selectedRow);
  }

}
