import { PqGridComponent } from './pq-grid/pq-grid.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'pq-grid', pathMatch: 'full' },
  { path: 'pq-grid', component: PqGridComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PqGridRoutingModule { }
